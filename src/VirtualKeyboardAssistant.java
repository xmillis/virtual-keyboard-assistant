import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class VirtualKeyboardAssistant {

	private static String VERSION = "1.0.1";

	private static List<String> mIterations;

	private static int mCurrentIterationIndex = -1;

	private static Robot sRobot;

	public static void main(String[] args)
			throws IOException, InterruptedException, AWTException, IllegalArgumentException, IllegalAccessException,
			NoSuchFieldException, SecurityException, ClassNotFoundException {
		if (args.length == 0 || args[0].startsWith("-?") || args[0].startsWith("-help")) {
			printUsage();
			return;
		}
		if (args[0].startsWith("-version")) {
			System.out.println(VERSION);
			return;
		}
		try {
			sRobot = new Robot();
		} catch (AWTException e) {
			e.printStackTrace();
		}
		while (mCurrentIterationIndex < 0 || (mIterations != null && mCurrentIterationIndex + 1 < mIterations.size())) {
			for (int i = 0; i < args.length; i++) {
				String arg = args[i];
				if (arg.startsWith("-iterate")) {
					if (mCurrentIterationIndex < 0) {
						mIterations = new ArrayList<String>();
						while (!(arg = args[++i]).startsWith("-")) {
							mIterations.add(arg);
						}
						if (mIterations.size()==1) {
							try {
								int noOfIterations = Integer.parseInt(mIterations.get(0));
								mIterations.clear();
								for (;noOfIterations>0;noOfIterations--) {
									mIterations.add(String.valueOf(noOfIterations));
								}
							} catch (NumberFormatException nfe) {
								// do nothing, this is not a number
							}
						}
					} else {
						while (!(arg = args[++i]).startsWith("-")) {
							// consume the iteration strings without doing anything, they are already in the
							// list
						}
					}
					i--;
					mCurrentIterationIndex++;
				} else if (arg.startsWith("-press")) {
					while (i < args.length - 1 && !(arg = args[++i]).startsWith("-")) {
						System.out.println("Pressing " + arg.toUpperCase());
						String[] keys = arg.split("\\+");
						press(keys);
					}
					if (arg.startsWith("-")) {
						i--;
					}
				} else if (arg.startsWith("-type")) {
					arg = args[++i];
					arg = injectIteration(arg);
					type(arg);
				} else if (arg.startsWith("-sleep")) {
					arg = args[++i];
					long time = Long.valueOf(arg);
					System.out.println("Sleeping " + time + "ms");
					Thread.sleep(time);
				} else if (arg.startsWith("-open")) {
					arg = args[++i];
					arg = injectIteration(arg);
					System.out.println("Opening " + arg);
					Desktop.getDesktop().browse(URI.create(arg));
				} else if (arg.startsWith("-click")) {
					String x = args[++i];
					x = injectIteration(x);
					String y = args[++i];
					y = injectIteration(y);
					click(Integer.valueOf(x), Integer.valueOf(y));
				} else if (arg.startsWith("-rightclick")) {
					String x = args[++i];
					x = injectIteration(x);
					String y = args[++i];
					y = injectIteration(y);
					rightclick(Integer.valueOf(x), Integer.valueOf(y));
				} else if (arg.startsWith("-")) {
					System.out.println(arg + " is unrecognized!!!");
					System.exit(-1);
				} else {
					System.out.println("Ignoring " + arg + " --- not sure what to do with this");
				}
			}
			if (mCurrentIterationIndex < 0) {
				mCurrentIterationIndex++;
			}
			Thread.sleep(1000);
		}
	}

	private static String injectIteration(String arg) {
		if (mIterations != null) {
			String iteration = mIterations.get(mCurrentIterationIndex);
			return arg.replace("#iteration#", iteration);
		}
		return arg;

	}

	private static void press(String[] keys) throws AWTException, IllegalArgumentException, IllegalAccessException,
			NoSuchFieldException, SecurityException, ClassNotFoundException, InterruptedException {
		int[] keyCodes = new int[keys.length];
		Class<?> keyEventClass = Class.forName("java.awt.event.KeyEvent");
		for (int i = 0; i < keys.length; i++) {
			String key = keys[i] = keys[i].trim().toUpperCase();
			if ("CTRL".equals(key)) {
				key = "CONTROL";
			}
			int keyCode = keyCodes[i] = keyEventClass.getField("VK_" + key).getInt(null);
			sRobot.keyPress(keyCode);
		}
		for (int i = 0; i < keys.length; i++) {
			sRobot.keyRelease(keyCodes[i]);
		}
		Thread.sleep(75);
	}

	private static void type(String string) throws AWTException, InterruptedException {
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			boolean isUppercase = Character.isUpperCase(c); 
			c = Character.toLowerCase(c);
			if (isUppercase) {
				sRobot.keyPress(KeyEvent.VK_SHIFT);
			}
			int keyCode;
			if (Character.isLetter(c)) {
				keyCode = (byte) c - 32;
			} else if (c == '!') {
				keyCode = KeyEvent.VK_1;
				sRobot.keyPress(KeyEvent.VK_SHIFT);
				isUppercase = true;
			} else if (c == '?') {
				keyCode = KeyEvent.VK_SLASH;
				sRobot.keyPress(KeyEvent.VK_SHIFT);
				isUppercase = true;
			} else if (c == ':') {
				keyCode = KeyEvent.VK_SEMICOLON;
				sRobot.keyPress(KeyEvent.VK_SHIFT);
				isUppercase = true;
			} else if (c == '"') {
				keyCode = KeyEvent.VK_QUOTE;
				sRobot.keyPress(KeyEvent.VK_SHIFT);
				isUppercase = true;
			} else {
				keyCode = (byte) c;
			}
			sRobot.keyPress(keyCode);
			sRobot.keyRelease(keyCode);
			if (isUppercase) {
				sRobot.keyRelease(KeyEvent.VK_SHIFT);
			}
			Thread.sleep(100);
		}
	}

	private static void click(int x, int y) throws AWTException, InterruptedException {
		mouseMove(x, y);
		sRobot.mousePress(InputEvent.BUTTON1_MASK);
		sRobot.mouseRelease(InputEvent.BUTTON1_MASK);
		Thread.sleep(1500);
	}

	private static void rightclick(int x, int y) throws AWTException, InterruptedException {
		mouseMove(x, y);
		sRobot.mousePress(InputEvent.BUTTON3_MASK);
		sRobot.mouseRelease(InputEvent.BUTTON3_MASK);
	}

	private static void mouseMove(int x, int y) throws AWTException, InterruptedException {
		Point point = MouseInfo.getPointerInfo().getLocation();
		if (x == point.getX() && y == point.getY()) {
			return;
		}
		boolean roundXDown = x < point.getX();
		boolean roundYDown = y < point.getY();
		double newX = (point.getX() + x) / 2;
		double newY = (point.getY() + y) / 2;
		int newRoundedX = (int) (roundXDown ? Math.floor(newX) : Math.ceil(newX));
		int newRoundedY = (int) (roundYDown ? Math.floor(newY) : Math.ceil(newY));
		Thread.sleep(125);
		sRobot.mouseMove(newRoundedX, newRoundedY);
		mouseMove(x, y);
	}
	
	private static void printUsage() {
		System.out.println("Usage: VirtualKeyboardAssistant.bat [-iterate...] [actions...]\r\n" + 
				"	-iterate <arguments which will act as strings referrable by #iteration#|a number specifying how many times the actions should be executed>\r\n" + 
				"		for example:\r\n" + 
				"			-iterate 5\r\n" + 
				"				will produce all the actions 5 times\r\n" + 
				"			-iterate \"My 1st string\" \"My2nd string\"\r\n" + 
				"				will produce all the actions 2 times and will allow actions such as -type to refer the #iteration# string\r\n" + 
				"actions include:\r\n" + 
				"	-open <url|uri|path> \r\n" + 
				"		will open the given location in the default app\r\n" + 
				"	-press [space separated list of keys or key combinations]\r\n" + 
				"		for example: -press ENTER ALT+F4\r\n" + 
				"	-type <a single argument representing a word or sentence to be typed>\r\n" + 
				"		for example: -type \"here is a sentence\"\r\n" + 
				"	-click <X> <Y>\r\n" + 
				"		where X and Y are numbers which represent the distance in pixels to the upper left corner\r\n" + 
				"	-rightclick <X> <Y>\r\n" + 
				"		same as -click but rightclicks\r\n" + 
				"	-sleep <millis>\r\n" + 
				"		will sleep for so many milliseconds");
	}
}
