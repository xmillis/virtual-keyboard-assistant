# README #

This virtual keyboard assistant will type stuff, press keys, click or rightclick certain points on the screen according to the instructions that you supply. I have built it for personal reasons, noticing that many times we do a lot of "repetitive boilerplate" actions around sometimes pretty tiny pieces of original information / content to achieve some effect, which is pretty inefficient. When i have such things to be done, i would much rather input the original content in some app and have it execute this repetitive boilerplate for me while i have a cup of coffee.

As a trivial example just to get the point across, if i want to add 5 or 10 notes to Google Keep from my PC, i have to do this sequence of operations:
* type original content
* click OK
* click text box to re-focus cursor for the next note - and here we go again
* type original content
* click OK
* click text box to re-focus cursor for the next note - and here we go again
* type original content
* click OK
* click text box to re-focus cursor for the next note - and here we go again
* type original content
* click OK
* click text box to re-focus cursor for the next note - and here we go again
* type original content
* click OK
* click text box to re-focus cursor for the next note - and here we go again
...
Not only is the original content an extremely small part of this effort, but in addition to the boilerplate effort we subject our mind/body connection to the click-followed-by-type-followed-by-click-followed-by-type fragmentation. I would much rather write a one-time-template which i can then execute forever:

	VirtualKeyboardAssistant.bat
	-iterate "My 1st note" "My 2nd note" "My 3rd" "My 4th" "My 5th"
	-open https://keep.google.com/#home
	-sleep 5000
	-type "#iteration# and some extra stuff"
	-click 1340 300
	-press alt+f4

### Download / How to run ###

Just download from https://bitbucket.org/xmillis/virtual-keyboard-assistant/downloads/ , unzip it anywhere and make sure you have a Java bin in your PATH before you launch. That's it! And here's a video that shows a running sample: https://www.youtube.com/watch?v=ONH7SDSExGg

	Usage: VirtualKeyboardAssistant.bat [-iterate...] [actions...]
	-iterate <arguments which will act as strings referrable by #iteration#|a number specifying how many times the actions should be executed>
		for example:
			-iterate 5
				will produce all the actions 5 times
			-iterate "My 1st string" "My2nd string"
				will produce all the actions 2 times and will allow actions such as -type to refer the #iteration# string
	actions include:
	-open <url|uri|path> 
		will open the given location in the default app
	-press [space separated list of keys or key combinations]
		for example: -press ENTER ALT+F4
	-type <a single argument representing a word or sentence to be typed>
		for example: -type "here is a sentence"
	-click <X> <Y>
		where X and Y are numbers which represent the distance in pixels to the upper left corner
	-rightclick <X> <Y>
		same as -click but rightclicks
	-sleep <millis>
		will sleep for so many milliseconds
	
### What is this repository for? ###

This site is basically just a home for this small tool. It's already powerful enough and i'm not sure how it could be extended (perhaps Linux shell scripts for convenience?) But if someone has any ideas or wants to contribute in some way, please contact me at email@teofilimon.com

### License (MIT variant) ###

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

1. The following link and this entire permission notice / license text shall be included in all copies or substantial portions of the Software:
	
	https://bitbucket.org/xmillis/virtual-keyboard-assistant

2. If the code is modified (at all) and distributed with modifications, the following text shall be included instead of the simple link (but still acompanied by this entire permission notice):

	<Software> is using a modified version of https://bitbucket.org/xmillis/virtual-keyboard-assistant

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

### Who do I talk to? ###

You can contact me at email@teofilimon.com if you wish. You can find out more about me at https://teofilimon.com